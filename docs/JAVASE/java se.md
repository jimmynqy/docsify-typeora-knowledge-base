## 注解 Java.Annotation

### 1、内置注解三个

```java
@Override	覆写超类方法
@Deprecated		过时方法
@SuppressWarnings("all")	取消代码警告
```

### 2、元注解

负责注解 其他注解

```JAVA
@Target("xxx")		注解的可生效位置
@Retention("xxx")	何时生效，参数（SOURCE<CLASS<RUNTIME）
@Document
@Inherited	子类可以继承父类中的该注解    
```

例子，自定会一个注解

```java
public class test{
    @MyAnnotation(value = "20201013")
	public void test(){
        
    }
}

@Target(value = {ElementType.METHOD,ElementType.type})	//方法，类	
@Retention(value = RetentionPolicy.RUNTIME)	
@Document
@Inherited	
@Interface MyAnnotation{ //自定义注解
    //默认后面加上括号，但不表示方法，仅表示参数，可加默认值
	String name() default "nqy";
    int age() default 0;
    String[] schools() default {"a","b"};
}
```



## 反射机制 Java.Reflection

Java是静态语言，但通过反射变成了准动态语言。而注解是通过反射机制读取的

### 1、运行

反射机制允许程序在执行过程中通过reflect API获得任何类的内部信息。其源头是Object类中的getClass()方法

```java
Class c = Class.forName("java.lang.String")

建类正常方式 ： import引入-->new实例化-->取得对象
反射方式 ： 实例化对象-->getClass()方法-->得到完整的包类名称    
```

### 2、功能

```
反射机制提供的功能
	在运行时判断一个对象的类
	在运行时构造任意一个类的对象
	...判断一个类所具有的成员变量和方法
	...获取泛型
	...条用任意一个对象的成员变量和方法
	...处理注解
	生成动态代理
```

### 3、实例

一个类在内存中只有一个class，所以无论Class.forName一个类几次，hashCode相同

```java
@SpringBootTest
class Springboot01HelloworldApplicationTests {

    @Test
    void contextLoads() throws ClassNotFoundException{
         //创建方式三种，User.class最为安全
        Class<?> aClass = Class.forName("com.nqy.pojo.User");
        Class<User> userClass = User.class; //通过类名获得
        Class<? extends User> aClass2 = new User().getClass(); //通过对象获得
        
        //常用方法
        aClass.getClass();
        Object o = aClass.newInstance();
        aClass.getName();
        aClass.getInterfaces();
        aClass.getSuperclass();	//获取父类名称，即com.nqy.pojo.User
    }
}
```

### 4、所有类型的Class

```java
@SpringBootTest
class Springboot01HelloworldApplicationTests {

	@Test
    void contextLoads() throws ClassNotFoundException,IllegalAccessException,InstantiationException{
        Class<Object> objectClass = Object.class;
        Class<Comparable> comparableClass = Comparable.class;
        Class<String[]> aClass1 = String[].class;
        Class<int[][]> aClass3 = int[][].class;
        Class<Override> overrideClass = Override.class;
        Class<ElementType> elementTypeClass = ElementType.class;
        Class<Integer> integerClass = Integer.class;
        Class<Void> voidClass = void.class;
        Class<Class> classClass = Class.class;
    }
}    
```

### 5、类加载内存过程

cmd执行java类时

javac HelloWorld.java  ---编译为class

java HelloWorld  --执行

==所有的类都是加载到内存==

```java
package com.nqy.test;

import org.junit.Test;

/**
 * @Author: nqy
 * @Description:
 * @Date: Created in 14:19 2020/10/14
 */
public class classLoaderTest {

    @Test
    public void test(){
        A a = new A();
        System.out.println("m最终的值:"+a.m);
    }
}

class A{
    static  {
        System.out.println("A类的静态代码块初始化");
        m = 300;
    }

    static int m = 100; //即使是静态变量赋值，也是在无参构造之后执行的

    public A() {
        System.out.println("A类的无参构造");
    }
}

输出结果：
    A类的静态代码块初始化
	A类的无参构造
	m最终的值:100
```

![image-20201014143344326](C:\Mine\Ufo\Note\JAVASE\java se.assets\image-20201014143344326.png)

##### 5.1、附整张JVM执行图

![image-20201014145112834](C:\Mine\Ufo\Note\JAVASE\java se.assets\image-20201014145112834.png)

![image-20201014144845214](C:\Mine\Ufo\Note\JAVASE\java se.assets\image-20201014144845214.png)

![image-20201014144918818](C:\Mine\Ufo\Note\JAVASE\java se.assets\image-20201014144918818.png)

##### 5.2、类的主动引用与被动引用

![image-20201014150932075](C:\Mine\Ufo\Note\JAVASE\java se.assets\image-20201014150932075.png)

1. 主动引用（会引起类的初始化）

   ```java
   public class classLoaderTest1 {
       static{
           System.out.println("main类被加载");
       }
   
       @Test
       public void test() throws ClassNotFoundException{
           //创建实例
           //Son son = new Son();
           System.out.println("=====================");
           //用反射方式
           //Class<Son> sonClass = Son.class;
           Class<?> aClass = Class.forName("com.nqy.test.Son");
       }
   }
   
   class Father{
       static {
           System.out.println("父类被加载");
       }
   }
   
   class Son extends Father{
       static {
           System.out.println("子类被加载");
       }
   }
   
   结果：
       main类被加载
   	=====================
   	父类被加载
   	子类被加载
   ```

   

2. 被动引用

   ```java
   public class classLoaderTest1 {
       static{
           System.out.println("main类被加载");
       }
   
       @Test
       public void test() throws ClassNotFoundException{
           //调用父类静态变量(子类没有主动申明，子类不会被初始化)
           System.out.println(Son.b);
   
           //数组，虽然和class名字一样，但数组不是class(只有main被初始化)
           Son[] sons = new Son[100];
   
           //调用该类的常量池final数据
           System.out.println(Son.mm);
       }
   }
   
   class Father{
       static {
           System.out.println("父类被加载");
       }
   
       static int b = 2;
   }
   
   class Son extends Father{
       static {
           System.out.println("子类被加载");
           m = 300;
       }
   
       static int m = 100;
       static final int mm = 1;
   }
   
   结果：（Son.b）
       main类被加载
   	父类被加载
   	2
   ```

### 6、类加载器

##### 6.1、附类加载器结构图

![image-20201014173835679](C:\Mine\Ufo\Note\JAVASE\java se.assets\image-20201014173835679.png)

##### 6.2、